#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
 
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
 
import feedparser 
import time
 
class Item(db.Model): 
    title = db.StringProperty(required=False)
    link = db.StringProperty(required=False)
    date = db.StringProperty(required=False)
 
class Scrawler(webapp.RequestHandler):
    
    def get(self):
        self.read_feed()      
        self.response.out.write(self.print_items())
        
    def read_feed(self):
        
        feeds = feedparser.parse( "http://www.techrepublic.com/search?t=14&o=1&mode=rss" )
        
        for feed in feeds[ "items" ]:
            querry = Item.gql("WHERE link = :1", feed[ "link" ])
            if(querry.count() == 0):
                item = Item()
                item.title = feed[ "title" ]
                item.link = feed[ "link" ]
                item.date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(time.time()))
                item.put()
    
    def print_items(self):
        s = "All items:<br>"
        for item in Item.all():
            s += item.date + " - <a href='" + item.link + "'>" + item.title + "</a><br>"
        return s
 
application = webapp.WSGIApplication([('/', Scrawler)], debug=True)
 
def main():
    run_wsgi_app(application)
 
if __name__ == "__main__":
    main() 